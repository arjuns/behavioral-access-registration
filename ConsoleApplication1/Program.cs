﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ConsoleApplication1
{
    class Program
    {
        private static void Main(string[] args)
        {
            User usr = new User() { Role = "admin", Id = 1 };
            RoomCategory cat = new RoomCategory() { OwnerId = 2 };

            AccessRegistry registration = new AccessRegistry();

            var canPerform = registration.CanPerform(usr, cat, Access.Write);
        }
    }

    public enum Access
    {
        Read,
        Write
    }

    public class Registration<T> : IRegistration where T : IOwnersProperty
    {
        public string Role { get; set; }
        public Access Access { get; set; }
        public Expression<Func<User, T, bool>> Expression { get; set; }

        LambdaExpression IRegistration.RawExpression
        {
            get { return this.Expression; }
        }



        Type IRegistration.ResourceType
        {
            get { return typeof(T); }
        }
    }

    public interface IRegistration
    {
        Type ResourceType { get; }
        string Role { get; }
        Access Access { get; }
        LambdaExpression RawExpression { get; }

    }

    public class AccessRegistry
    {
        private readonly HashSet<IRegistration> registrations;

        public AccessRegistry()
        {
            this.registrations = new HashSet<IRegistration>();

            Registration<RoomCategory> registration = Role("admin")
                .WriteAccessTo<RoomCategory>()
                .When((user, room) => room.OwnerId == user.Id);

            this.registrations.Add(registration);
        }

        public static IRoleExpression Role(string role)
        {
            return new RoleExpression(role);
        }

        public bool CanPerform<T>(User usr, T cat, Access access) where T : IOwnersProperty
        {
            var reg = this.registrations.FirstOrDefault(x =>
                x.ResourceType == typeof(T)
                && x.Role == usr.Role
                && x.Access == access
                );
            if (reg != null)
            {
                return (bool)reg.RawExpression.Compile().DynamicInvoke(usr, cat);
            }
            return false;
        }

        public bool CanWrite<T>(User usr, T cat) where T : IOwnersProperty
        {
            return CanPerform(usr, cat, Access.Write);
        }
    }


    public class RoomCategory : IOwnersProperty
    {
        public int OwnerId { get; set; }
    }

    public class User
    {
        public int Id { get; set; }
        public string Role { get; set; }
    }

    public interface IOwnersProperty
    {
        int OwnerId { get; set; }
    }
}
