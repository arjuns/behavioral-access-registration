﻿namespace ConsoleApplication1
{
    public interface IRoleExpression
    {
        string Role { get; }
        IWriteAccessExpression<T> WriteAccessTo<T>() where T : IOwnersProperty;

    }

    public class RoleExpression : IRoleExpression
    {
        private readonly string role;

        public RoleExpression(string role)
        {

            this.role = role;
        }

        public string Role
        {
            get { return this.role; }
        }

        public IWriteAccessExpression<T> WriteAccessTo<T>() where T : IOwnersProperty
        {
            return new WriteAccessExpression<T>(this);
        }
    }
}