﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ConsoleApplication1
{
    public interface IWriteAccessExpression<T> where T : IOwnersProperty
    {
        Registration<T> When(Expression<Func<User, T, bool>> predicate);
    }

    public class WriteAccessExpression<T> : IWriteAccessExpression<T> where T :  IOwnersProperty
    {
        private readonly IRoleExpression expr;

        public WriteAccessExpression(IRoleExpression expr)
        {
            this.expr = expr;
        }

        public Registration<T> When(Expression<Func<User, T, bool>> predicate)
        {
            return new Registration<T>()
            {
                Access = Access.Write,
                Expression = predicate ,
                Role = expr.Role,
            };
        }
    }
}